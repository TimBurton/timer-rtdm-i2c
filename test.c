#include <linux/version.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <linux/fs.h>

#include <asm/uaccess.h>
#include <rtdm/driver.h>
#include <rtdm/rtdm.h>


static int periode_us = 10000;

static void appel(void);
static void timer_oscillateur(rtdm_timer_t *);
static rtdm_timer_t rtimer;


static int __init init_oscillateur (void)
{
	int err;

	rtdm_printk(KERN_INFO "%s.%s() : Initialisation\n", THIS_MODULE->name, __FUNCTION__);
	
	if ((err = rtdm_timer_init(& rtimer, timer_oscillateur, "Oscillateur")) != 0) {
		rtdm_printk(KERN_INFO "%s.%s() : Erreur1\n", THIS_MODULE->name, __FUNCTION__);
		return err;
	}
	
	if ((err = rtdm_timer_start(& rtimer, periode_us*1000, periode_us*1000, RTDM_TIMERMODE_RELATIVE)) != 0) {
            rtdm_printk(KERN_INFO "%s.%s() : Erreur2\n", THIS_MODULE->name, __FUNCTION__);
		rtdm_timer_destroy(& rtimer);
		return err;
	}

    rtdm_printk(KERN_INFO "%s.%s() : Fin  d'initialisation\n", THIS_MODULE->name, __FUNCTION__);

	return 0; 
}



static void __exit exit_oscillateur (void)
{
	rtdm_timer_stop(& rtimer);
	rtdm_timer_destroy(& rtimer);
    rtdm_printk(KERN_INFO "%s.%s() : Destruction\n", THIS_MODULE->name, __FUNCTION__);
}

void appel(void){
    rtdm_printk(KERN_INFO "%s.%s() : TICK \n", THIS_MODULE->name, __FUNCTION__);
}

static void timer_oscillateur(rtdm_timer_t * unused)
{
	appel();
}


module_init(init_oscillateur);
module_exit(exit_oscillateur);
MODULE_LICENSE("GPL");

