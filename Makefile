ifneq ($(KERNELRELEASE),)
	obj-m += test.o
#        obj-m += oscillateur-gpio-timer.o
#        obj-m += oscillateur-gpio-hrtimer.o


	EXTRA_CFLAGS := -I /usr/xenomai/include/
else

	XENOCONFIG=/usr/xenomai/bin/xeno-config
	CC=$(shell      $(XENOCONFIG) --cc)
	CFLAGS=$(shell  $(XENOCONFIG) --skin=posix --cflags)
	LDFLAGS=$(shell $(XENOCONFIG) --skin=posix --ldflags)
	LIBDIR=$(shell  $(XENOCONFIG) --skin=posix --libdir)

	CROSS_COMPILE ?=
	KERNEL_DIR ?= /usr/src/linux
	MODULE_DIR := $(shell pwd)

.PHONY: all

.PHONY: modules
modules:
	$(MAKE) -C $(KERNEL_DIR) SUBDIRS=$(MODULE_DIR) CROSS_COMPILE=$(CROSS_COMPILE) modules

.PHONY: clean
clean::
	rm -f  *.o  .*.o  .*.o.* *.ko  .*.ko  *.mod.* .*.mod.* .*.cmd *~
	rm -f Module.symvers Module.markers modules.order
	rm -rf .tmp_versions
endif